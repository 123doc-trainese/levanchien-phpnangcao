<?php
// Named Arguments
//function foo($first, $second)
//{
//    return $first + $second;
//}
//
//echo foo(second: 2, first: 1) . "\n";//Khong can quan tam thu tu, de dang document
//====================================================================

// Khai bao thuoc tinh ngay ham khoi tao (khong dung duoc cho abstract classes va interfaces
//class Point
//{
//    public function __construct(
//        public float $x = 0.0,
//        public float $y = 0.0,
//        public float $z = 0.0,
//    )
//    {
//    }
//}
//
//$point = new Point(1, 2, 3);
//echo $point->x;
//====================================================================

// Match expression
//try {
//    $expressionResult = match (5) {
//        1, 2 => 'first',
//        3, 4 => 'second',
//        default => 'default'
//    };
//    echo $expressionResult;
//} catch (\UnhandledMatchError $e) {
//    var_dump($e->getMessage());
//}

//====================================================================
// Trước đây ta có thể gán giá trị Null cho bất kì một biến nào. Tuy nhiên với PHP 8 thì phải chỉ định biến đó là có thể null hay không.
//class Test
//{
//
//    public function __construct(public ?float $prop = null)
//    {
//    }
//}
//
//$test = new Test();
//echo $test->prop;

//====================================================================
// Nullsafe operator
//$country = $session?->user?->getAddress()?->country;

//====================================================================
//Hầu hết các function nội bộ hiện ném một ngoại lệ Lỗi nếu việc xác nhận các tham số không thành công.
//
//PHP 7:
//strlen([]); // Warning: strlen() expects parameter 1 to be string, array given
//array_chunk([], -1); // Warning: array_chunk(): Size parameter expected to be greater than 0

//PHP 8:
//strlen([]); // TypeError: strlen(): Argument #1 ($str) must be of type string, array given
//array_chunk([], -1); // ValueError: array_chunk(): Argument #2 ($length) must be greater than 0
//====================================================================
