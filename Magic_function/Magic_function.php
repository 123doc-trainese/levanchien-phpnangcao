<?php
// Magic methods là các phương thức đặc biệt được tạo ra nhằm giải quyết các vấn đề về sự kiện trong chương trình
// Ưu điểm: Tùy biến hành vi, tạo đối tượng theo mong muốn
// Nhược điểm: chậm hơn so với phương thức bình thường.
class Test
{
    private $prop1;
    private $prop2;

    public function __construct()
    {
//        echo "construct" . "\n";
    }

    public function __destruct()
    {
//        echo "destroy" . "\n";
    }

    public function __set($key, $value)
    {
        if (property_exists($this, $key)) {
            echo "set" . "\n";
            $this->prop1 = $value;
        } else {
            echo "Invalid prop";
        }
    }

    public function __get($key)
    {
        if (property_exists($this, $key)) {
            echo "get" . "\n";
            return $this->prop1;
        } else {
            echo "Invalid prop";
        }
    }

    public function __isset($key)
    {
        echo "Isset";
    }

    public function __unset($key)
    {

    }

    private function getProp1()
    {
        return $this->prop1;
    }

    public function __call($methodName, $arguments)
    {
        echo "__call";
    }

    public static function __callStatic($methidName, $arguments)
    {
        echo "call no access static method";
    }

    public function __sleep() // Quy dinh thuoc tinh tra ve khi serialize()
    {
        return ['prop1'];
    }

    public function __wakeup() // Goi khi unserialize()
    {

    }

    public function __toString()// Duoc goi khi dung object nhu la 1 string
    {
        return "call toString";
    }

    public function __invoke() // Duoc goi khi su dung doi tuong nhu mot ham, $argument neu trong co thi bo trong
    {
        echo "call invoke" . "\n";
    }

    public function __clone(): void // Duoc goi khi clone object
    {
        echo "call clone";
    }

    public function __debugInfo(): ?array // Tuy chinh thong so tra ve khi goi var_dump()
    {
        return ['prop1' => $this->prop1];
    }
}

$test = new Test();
$test2 = clone $test;
