<?php
$arr = ['one' => 1, 'two' => 2, 'three' => 3, 'four' => 4];
$arr2 = ['four' => 4, 'nam' => 5, 'sau' => 6];
//echo array_shift($arr) . "\n";// Xoa phan tu dau cua mang va tra ve phan tu day
//echo array_unshift($arr, 'bon', 'nam'); // Them mot hoac nhieu phan tu vao mang, tra ve so phan tu sau khi them
//print_r(array_flip($arr)); // Tra ve mang dao nguoc $key - $value
//sort($arr);//rsort, asort, ksort, arsort, krsort
//print_r(array_reverse($arr)); // Dao nguoc lai vi tri
//print_r(array_merge($arr, $arr2)); // Gop hai hay nhieu mang
//print_r(array_combine($arr, $arr2)); // Tao mang tu mang key va mang value
//echo array_search(4, $arr); // Tim kiem value tra ve key dau tien
//print_r(array_slice($arr, -3, 3)); // Lay ra $lengtht phan tu bat dau tu $begin
//print_r(array_diff($arr, $arr2)); // arr - arr2 (theo value)
//print_r(array_diff_assoc($arr, $arr2)); // arr - arr2 (theo key)
//print_r(array_intersect_assoc($arr, $arr2)); // giao, $key => $value
//print_r(array_chunk($arr, 2)); // Chia mang thanh mang cac mang con 2 phan tu
//print_r(array_filter($arr, function ($value, $key) {
//    if ($value % 2 == 0) {
//        return 1;
//    }
//}, ARRAY_FILTER_USE_BOTH));
//echo array_reduce($arr, function ($carry, $item) {
//    return $carry + $item;
//});
// array_map()
//print_r($arr + $arr2);