# LeVanChien-PHPNangCao
- Tìm hiểu về Magic function: `__contruct`, `__destruct`, `__set`, `__get`, `__isset`, `__unset`, `__call`, `__callStatic`, `__sleep`, `__wakeup`, `__toString`, `__invoke`, `__clone`, `__debugInfo`.
- Tìm hiểu thuật toán được sử dụng bên trong một số hàm builtin php : sắp xếp, so sánh, tìm kiếm, thay thế, ...: `array_shift`, `array_unshift`, `array_flip`, `sort`, `array_reverse`, `array_merge`, `array_combine`, `array_search`, `array_slice`, `array_diff`, `array_diff_assoc`, `array_intersect_assoc`, `array_chunk`, `array_filter`, `array_reduce`, `array_map`.
- Tìm hiểu các điểm mới trong phiên bản PHP mới nhất so với các bản còn trong thời gian support:
    - Named Arguments
    - Khai bao thuoc tinh ngay ham khoi tao (khong dung duoc cho abstract classes va interfaces
    - Match expression
    - Nullsafe operator
    - Hầu hết các function nội bộ hiện ném một ngoại lệ Lỗi nếu việc xác nhận các tham số không thành công.

